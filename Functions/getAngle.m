function [angle,radian]=getAngle(pointO,pointP)
%Input O-(x1,y1) and P(x2,y2)points
%Output Angle
%devloper Er.Abbas Manthiri S
%Email-abbasmanthiribe@gmail.com

%Angle get accurate if line length is high
x1=pointO(1);
x2=pointP(1);
y1=pointO(2);
y2=pointP(2);
x=(x2-x1);
y=(y2-y1);
z=sqrt(x^2+y^2);
d=x/z;
d1=y/z;
if y2==abs(y2)
    angle=acosd(d);
else
    if x2==abs(x2)
        angle=360+asind(d1);
    else
        angle=360-acosd(d);
    end
end
radian=angle*pi/180;