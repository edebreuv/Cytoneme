function ...
    [cell_mask, cell_is_on_left] = ...
    Mask_Cell(...
        background)

global no_keypress_yet_g, no_keypress_yet_g = true;

standard_height = 400;
axis_hmargin    = 15;
axis_vmargin    = 15;

[ho_figure, ho_axes] = FigureAndAxes(background, standard_height, axis_hmargin, axis_vmargin);
freehand_obj    = imfreehand(ho_axes, 'Closed', false);
frontier_object = iptgetapi(freehand_obj);

while ishandle(ho_figure) && no_keypress_yet_g
    uiwait(ho_figure)
end
if isempty(frontier_object)
    frontier = [];
else
    frontier = frontier_object.getPosition();
end
if ishandle(ho_figure)
    close(ho_figure)
end

if isempty(frontier)
    cell_mask       = [];
    cell_is_on_left = [];
    return
end

frontier = round(fliplr(frontier)); % Flip to put in row/col order
for idx = 1:size(frontier,1)
    frontier(idx,1) = max(frontier(idx,1), 1);
    frontier(idx,2) = max(frontier(idx,2), 1);
    frontier(idx,1) = min(frontier(idx,1), size(background, 1));
    frontier(idx,2) = min(frontier(idx,2), size(background, 2));
end
if frontier(1,1) > 1
    frontier = [1, frontier(1,2); frontier];
end
if frontier(end,1) < size(background, 1)
    frontier = [frontier; size(background, 1), frontier(end,2)];
end

cell_mask = false(size(background));

for idx = 1:(size(frontier,1)-1)
    [rows, cols] = bresenham(frontier(idx,1), frontier(idx,2), frontier(idx+1,1), frontier(idx+1,2));
    lin_idc = sub2ind(size(background), rows, cols);
    cell_mask(lin_idc) = 1;
end

for row = 1:size(cell_mask, 1)
    if cell_mask(row,1) == 0
        cell_mask = imfill(cell_mask, [row 1], 4);
        break
    end
end

average_left  = mean(background(cell_mask));
average_right = mean(background(~cell_mask));

cell_is_on_left = (average_left > average_right);
if ~cell_is_on_left
    cell_mask = ~cell_mask;
end

end



function ...
    [ho_figure, ho_axes] = ...
    FigureAndAxes(...
        background, standard_height, axis_hmargin, axis_vmargin)

    framesize = size(background);
    framesize = framesize([1 2]);
    ratio = framesize(2) / framesize(1);

    ho_figure = figure('NumberTitle', 'off', ...
        'Name', 'ROI selection (Press Return key when done)', ...
        'Color', 'white', 'Menubar', 'none', 'Toolbar', 'none', ...
        'Units', 'pixel', 'Position', [0, 0, axis_hmargin + (standard_height*ratio), standard_height + axis_vmargin]);
    movegui(ho_figure, 'center')

    ho_axes = axes('Parent', ho_figure, 'TickDir', 'out', ...
        'Units', 'pixel', 'Position', [axis_hmargin, axis_vmargin, standard_height*ratio, standard_height]);
    imagesc(background), colormap(gray)

    set(ho_figure, 'KeyPressFcn', @OnKeypress)
end



function ...
    OnKeypress(...
        hof, event)

    global no_keypress_yet_g

    if strcmp(event.Key, 'return')
        no_keypress_yet_g = false;
        uiresume(hof)
    end
end
