function [P1,y1,x1,Pend,y2,x2,M] = LeftmostEndPoint(edges, edge_idx, skeleton_mask, cell_is_on_left)

P1 = edges(edge_idx).point(1);
[y1,x1] = ind2sub(size(skeleton_mask), P1);

Pend = edges(edge_idx).point(end);
[y2,x2] = ind2sub(size(skeleton_mask), Pend);

if cell_is_on_left
    M = min([x1,x2]);
else
    M = max([x1,x2]);
end
