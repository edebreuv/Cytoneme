%% Cyto images
[FileName, PathName, ~] = uigetfile({'*.*'}, 'Select Cytomenes Image');
if FileName == 0
    fprintf('NO CYTONEME IMAGE SELECTED. NOTHING TO BE DONE\n')
    return
end
cyto_img_name = fullfile(PathName, FileName);
cyto_img_info = imfinfo(cyto_img_name);
num_cyto_stacks = numel(cyto_img_info);
fprintf('Image %dx%d with %d stacks (if many stacks, loading will take some time)\n', ...
        cyto_img_info(1).Width, cyto_img_info(1).Height, num_cyto_stacks)

cyto_img = [];
for stack_idx = 1:num_cyto_stacks
    cyto_img = cat(3, cyto_img, imread(cyto_img_name, stack_idx, 'Info', cyto_img_info));
end
% Maximun Intensity Projection (MIP)
cyto_mip = double(max(cyto_img, [], 3));
cyto_mip_dim = size(cyto_mip);

%% Membrane images
[FileName, PathName, ~] =  uigetfile({'*.*'}, 'Select Membrane Image (or Cancel if none)');
membrane_img_name = fullfile(PathName, FileName);

if membrane_img_name ~= 0
    membrane_img_info = imfinfo(membrane_img_name);
    num_membrane_stacks = numel(membrane_img_info);
    
    membrane_img = [];
    for k = 1:num_membrane_stacks
        membrane_img = cat(3, membrane_img, imread(membrane_img_name, k, 'Info', membrane_img_info));
    end
    membrane_mip = max(membrane_img, [], 3);
    membrane_mip_for_display = imadjust(membrane_mip, stretchlim(membrane_mip), []); % strech histogram
    
    figure, imagesc(membrane_mip_for_display), title('Adjusted Membrane')
    
    cols(:, 1) = round(1:cyto_mip_dim(1)/3);              % Extraction of the first third of the image
    cols(:, 2) = round(2*cyto_mip_dim(1)/3+1:cyto_mip_dim(1));     % Extraction of the last third of the image
    
    mean1 = mean(mean(cyto_mip(:, cols(:, 1))));       % Average of intensity for each extraction
    mean2 = mean(mean(cyto_mip(:, cols(:, 2))));
    
    [Max, IDX] = max([mean1, mean2]);            % Allows to select the side where we built the mask
    cell_is_on_left = (IDX == 1);
        
    mask_for_display = zeros(cyto_mip_dim); % (for best vizualization of location)    
    if IDX == 1
        mask_for_display(:, cols(end, IDX)) = 1;
    else
        mask_for_display(:, cols(1, IDX)) = 1;
    end
    tmp_for_display(:, :, 1) = membrane_mip_for_display;                 % vizualisation of the position of the initial contour
    tmp_for_display(:, :, 2) = 255 * mask_for_display;
    tmp_for_display(:, :, 3) = zeros(cyto_mip_dim);
    figure, imagesc(tmp_for_display), title('Initial Active Contour')
    
    intial_segm = zeros(cyto_mip_dim);
    intial_segm(:, cols(:, IDX)) = 1;
    segmentation = activecontour(membrane_mip_for_display, intial_segm, 5000, 'Chan-Vese', 'SmoothFactor', .5, 'ContractionBias', -.2);
    mask_cell = imclose(segmentation, strel('disk', 20));

    tmp_for_display(:, :, 1) = cyto_mip;                      % vizualisation of the position of the final contour on the initiql image
    tmp_for_display(:, :, 2) = 255 * mask_cell;
    tmp_for_display(:, :, 3) = zeros(cyto_mip_dim);
    figure, imagesc(tmp_for_display), title('Final Active Contour')    
else
%     mask_cell = imread('C:\Users\eric\Desktop\data\cyto-0-mask_cell.png');
%     cell_is_on_left = true;
    while (true)
        [mask_cell, cell_is_on_left] = Mask_Cell(cyto_mip);
        if isempty(mask_cell)
            fprintf('NO CELL MASK. NOTHING TO BE DONE\n')
            return
        end
        
        figure, imagesc(double(128*mask_cell)+double(cyto_mip)), colormap(gray)
        
        opts.Interpreter = 'tex';
        opts.Default = 'Yes';
        answer = questdlg('Are you satisfied with your membrane tracing ?', ...
                 'Confirmation', 'Yes', 'No', opts);
        if strcmp(answer, 'Yes')
            close(gcf)
            break
        end
        close(gcf)
    end
end

%% TIC
tic

%% Make sure cell is on the left-hand side
if ~cell_is_on_left
    fprintf('/!\\ CELL IS ON RIGHT SIDE:\n/!\\ ALL IMAGES WILL BE FLIPPED TO CELL ON LEFT SIDE\n')
    mask_cell = fliplr(mask_cell);
    cyto_img  = fliplr(cyto_img);
    cyto_mip  = fliplr(cyto_mip);
    if membrane_img_name ~= 0
        membrane_img = fliplr(membrane_img);
        membrane_mip = fliplr(membrane_mip);
        membrane_mip_for_display = fliplr(membrane_mip_for_display);
    end
    
    cell_is_on_left = true;
end
mask_cell = double(mask_cell > 0);
dilated_mask = imdilate(mask_cell, strel('square',3));
mask_cyto = 1.0 - mask_cell;

%% Next steps
fprintf('Next step(s): Image Processing\n')

%% Top hat (Transformation to enhance linear structures)
cyto_mip_enhanced = zeros(cyto_mip_dim);
for theta = 0:10:170
    current_angle = imtophat(cyto_mip, strel('line', 10, theta));
    cyto_mip_enhanced = max(cyto_mip_enhanced, current_angle);
end

%% Frangi filter
Options = struct('FrangiScaleRange', [1 3], ...
                 'FrangiScaleRatio', 0.5,  ...
                 'FrangiBetaOne',    0.5,  ...
                 'FrangiBetaTwo',    15,   ...
                 'verbose',          false, ...
                 'BlackWhite',       false);
cyto_mip_frangi = FrangiFilter2D(cyto_mip_enhanced, Options);
cyto_mip_thresholded = cyto_mip_frangi >= 0.25;
cyto_mip_thresholded = cyto_mip_thresholded .* mask_cyto;

figure, imagesc(cyto_mip_thresholded), title('Thresholded Frangi')

%% Cleaning binarized cyto mip
%SE = strel('disk', 1);
%cyto_mip_cleaned = imclose(cyto_mip_thresholded, SE); % Closing : Combination of dilation and erosion -> all that is smaller than the structuring element
%cyto_mip_cleaned = imopen(cyto_mip_cleaned, SE);      % Opening : Combination of erosion and dilation -> smoothing of the shape and denoising of the bottom
cyto_mip_cleaned = cyto_mip_thresholded;

%% Next steps
toc, fprintf('Next step(s): Skeletonization\n')

%% Cyto skeletonization
cyto_skeleton = bwmorph(cyto_mip_cleaned, 'skel', Inf);

%% Useless pixel removal in cyto skeleton
n_comp = bwconncomp(double(cyto_skeleton), 8);
n_comp = n_comp.NumObjects;
where_skel = find(cyto_skeleton > 0);
skel_wo_pixel = cyto_skeleton;

n_neighbors = zeros(cyto_mip_dim);
for row_shift = [-1, 0, 1]
    for col_shift = [-1, 0, 1]
        if (row_shift ~=  0) || (col_shift ~=  0)
            n_neighbors = n_neighbors + circshift(cyto_skeleton, [row_shift, col_shift]);
        end
    end
end

for idx = 1:length(where_skel)
    if n_neighbors(where_skel(idx)) > 1
        skel_wo_pixel(where_skel(idx)) = 0;
        new_n_comp = bwconncomp(skel_wo_pixel, 8);
        new_n_comp = new_n_comp.NumObjects;
        if new_n_comp == n_comp
            cyto_skeleton(where_skel(idx)) = 0;
            % /!\ code duplication (and brute force)
            n_neighbors(:) = 0;
            for row_shift = [-1, 0, 1]
                for col_shift = [-1, 0, 1]
                    if (row_shift ~=  0) || (col_shift ~=  0)
                        n_neighbors = n_neighbors + circshift(cyto_skeleton, [row_shift, col_shift]);
                    end
                end
            end
        else
            skel_wo_pixel(where_skel(idx)) = 1;
        end
    end
end

%% Next steps
toc, fprintf('Next step(s): Skeleton -> Graph\n')

%% Cyto skeleton to cyto graph
[~, skeleton_nodes, skeleton_edges] = Skel2Graph3D(cyto_skeleton, 0);

figure, hold on

for node_idx = 1:length(skeleton_nodes)
    current_node = skeleton_nodes(node_idx);
    
    x = current_node.comx;
    y = current_node.comy;
    if current_node.ep == 1
        node_color = 'r';
    else
        node_color = 'g';
    end
    
    incident_edges_idc = current_node.edges_idc;
    for edge_idx = 1:length(incident_edges_idc)
        current_edge = skeleton_edges(incident_edges_idc(edge_idx));
        if skeleton_nodes(current_edge.n2).ep == 1
            edge_color = 'b';
        elseif skeleton_nodes(current_edge.n1).ep == 1
            edge_color = 'c';
        else
            edge_color = 'r';
        end
        
        for idx = 1:length(current_edge.point) - 1
            [x3, y3] = ind2sub(cyto_mip_dim, current_edge.point(idx));
            [x2, y2] = ind2sub(cyto_mip_dim, current_edge.point(idx+1));
            line([y3, y2], [cyto_mip_dim(2) - x3, cyto_mip_dim(2) - x2], 'Color', edge_color, 'LineWidth', 2);
        end
    end
    
    plot(y, cyto_mip_dim(2) - x, 'o', 'Markersize', 4, ...
        'MarkerFaceColor', node_color, ...
        'Color', 'k');
end
set(gcf, 'Color', 'white'), axis equal, title('Skeleton Graph')

%% Next steps
toc, fprintf('Next step(s): Constrained Graph -> Skeleton\n')

%% Rebuild cyto skeleton from graph with geometrical constraints
end_points_idc = [];
for node_idx = 1:length(skeleton_nodes)
    if skeleton_nodes(node_idx).ep == 1
        end_points_idc = [end_points_idc; node_idx];
    end
end

end_point_coords = [];
for ep_idx = 1:length(end_points_idc)
    lin_idx = skeleton_nodes(end_points_idc(ep_idx)).idx; % linear idx
    [row, col] = ind2sub(cyto_mip_dim, lin_idx);
    end_point_coords = [end_point_coords; col, row, lin_idx];
end
end_point_coords = sortrows(end_point_coords, 'descend');

[skeleton_edges(:).assigned] = deal(0);
rebuilt_cyto_skeleton = zeros(cyto_mip_dim);
branch_idx = 1;

for ep_coords_idx = 1:length(end_point_coords)
    node_idx = cellfun(@(elm) any(elm == end_point_coords(ep_coords_idx, 3)), {skeleton_nodes.idx}, 'UniformOutput', false);
    node_idx = find([node_idx{:}]); assert(~isempty(node_idx))

    while true            
        current_node = skeleton_nodes(node_idx);
        idx_of_edge_to_visit = current_node.edges_idc;
        
        if isempty(idx_of_edge_to_visit)
            break
        elseif length(idx_of_edge_to_visit) > 1
            leftmost_ep_cols = [];
            for edge_idx = 1:length(idx_of_edge_to_visit)
                [~, ~, ~, ~, ~, ~, local_lm_col] = LeftmostEndPoint(skeleton_edges, idx_of_edge_to_visit(edge_idx), cyto_skeleton, cell_is_on_left);
                leftmost_ep_cols = [leftmost_ep_cols; local_lm_col];
            end
            [~, idc_of_leftmost] = min(leftmost_ep_cols);
            idx_of_edge_to_visit = idx_of_edge_to_visit(idc_of_leftmost(1));
        end
        
        current_edge = skeleton_edges(idx_of_edge_to_visit);
        if current_edge.assigned > 0
            break
        end

        if current_edge.n1 == node_idx
            if skeleton_nodes(current_edge.n2).comy > current_node.comy % wrong orientation
                break
            end
        else
            if skeleton_nodes(current_edge.n1).comy > current_node.comy % wrong orientation
                break
            end
        end

        rebuilt_cyto_skeleton(current_edge.point) = branch_idx;
        rebuilt_cyto_skeleton(current_node.idx)   = branch_idx;
        skeleton_edges(idx_of_edge_to_visit).assigned = branch_idx;
        branch_idx = branch_idx + 1;

        if node_idx == current_edge.n1
            node_idx = current_edge.n2;
        else
            node_idx = current_edge.n1;
        end
    end
end

rebuilt_cyto_skeleton_w_idc = rebuilt_cyto_skeleton;
rebuilt_cyto_skeleton = double(rebuilt_cyto_skeleton > 0);

tmp_for_display(:, :, 1) = uint8(cyto_mip);
tmp_for_display(:, :, 2) = 255 * uint8(rebuilt_cyto_skeleton);
tmp_for_display(:, :, 3) = 255 * uint8(mask_cell);
figure, imagesc(tmp_for_display), title('Skeleton Rebuilt From Graph')

%% Next steps
toc, fprintf('Next step(s): Gap Filling\n')

%% Gap Filling : Dijkstra
skeleton_connected_cmp = double(labelmatrix(bwconncomp(rebuilt_cyto_skeleton)));
n_connected_comp = max(skeleton_connected_cmp(:));

leftmost_ep_of_trees  = []; % Actually not necessarily end points since edges can have a hook shape
rightmost_ep_of_trees = [];
for label = 1:n_connected_comp
    [skeleton_rows, skeleton_cols] = find(skeleton_connected_cmp == label);
    sorted_skeleton_cols = sortrows([skeleton_cols skeleton_rows]);
    leftmost_ep_of_trees  = [leftmost_ep_of_trees;  sorted_skeleton_cols(1, :)];
    rightmost_ep_of_trees = [rightmost_ep_of_trees; sorted_skeleton_cols(end, :)];
end

gray_levels_on_skeleton = cyto_mip(rebuilt_cyto_skeleton > 0);
min_gray_on_skel = min(gray_levels_on_skeleton);
max_gray_on_skel = max(gray_levels_on_skeleton);

max_gap_length = 10;
max_gap_angle  = 45.0;
max_gap_cost   = 15.0;

%rejected_connections = rebuilt_cyto_skeleton;

for cc_one_idx = 1:(n_connected_comp - 1)
    for cc_two_idx = (cc_one_idx + 1):n_connected_comp
        leftmost_gap_coords_1  = rightmost_ep_of_trees(cc_one_idx, :); % Rightmost EP if leftmost point of gap
        rightmost_gap_coords_1 = leftmost_ep_of_trees(cc_two_idx, :);
        one_two_norm = norm(rightmost_gap_coords_1 - leftmost_gap_coords_1);

        leftmost_gap_coords_2  = rightmost_ep_of_trees(cc_two_idx, :); % Rightmost EP if leftmost point of gap
        rightmost_gap_coords_2 = leftmost_ep_of_trees(cc_one_idx, :);
        two_one_norm = norm(rightmost_gap_coords_2 - leftmost_gap_coords_2);

        if min(one_two_norm, two_one_norm) > max_gap_length
            continue
        end
        
        if one_two_norm < two_one_norm
            leftmost_gap_coords  = leftmost_gap_coords_1;
            rightmost_gap_coords = rightmost_gap_coords_1;
        else
            leftmost_gap_coords  = leftmost_gap_coords_2;
            rightmost_gap_coords = rightmost_gap_coords_2;
        end
        
        [angle_in_degrees, ~] = getAngle(leftmost_gap_coords, rightmost_gap_coords);
        if abs(angle_in_degrees) > max_gap_angle
            continue
        end

        if rightmost_gap_coords(2) >= leftmost_gap_coords(2)
            row_step = 1;
        else
            row_step = -1;
        end
        [all_rows, all_cols] = ndgrid(leftmost_gap_coords(2):row_step:rightmost_gap_coords(2), ...
                                      leftmost_gap_coords(1):rightmost_gap_coords(1));
        search_window_vertices = [all_cols(:), all_rows(:)];
        assert(rebuilt_cyto_skeleton(search_window_vertices(1,2),   search_window_vertices(1,1))   == 1)
        assert(rebuilt_cyto_skeleton(search_window_vertices(end,2), search_window_vertices(end,1)) == 1)

        for vertex_idx = 2:(size(search_window_vertices, 1) - 1)
            col = search_window_vertices(vertex_idx, 1);
            row = search_window_vertices(vertex_idx, 2);
            if rebuilt_cyto_skeleton(row, col) > 0
                continue
            end
        end
        
        n_edges = 0;
        edge_weights = zeros(size(search_window_vertices, 1) * (size(search_window_vertices, 1) - 1), 3);
        for p = 1:(size(search_window_vertices, 1) - 1)
            for q = (p+1):size(search_window_vertices, 1)
                edge_length = norm(search_window_vertices(p,:) - search_window_vertices(q,:));
                if edge_length < 1.5
                    n_edges = n_edges + 1;
                    edge_weights(n_edges, 1) = p;
                    edge_weights(n_edges, 2) = q;

                    gray_level = cyto_mip(search_window_vertices(q, 2), search_window_vertices(q, 1));
                    if gray_level < min_gray_on_skel
                        edge_weights(n_edges, 3) = edge_length + (min_gray_on_skel / max(1, gray_level));
                    elseif gray_level < max_gray_on_skel
                        edge_weights(n_edges, 3) = edge_length + (max_gray_on_skel - gray_level) / (max_gray_on_skel - min_gray_on_skel);
                    else
                        edge_weights(n_edges, 3) = edge_length;
                    end
                end
            end
        end

        [gap_cost, shortest_path] = dijkstra(search_window_vertices, edge_weights(1:n_edges, :), 1, size(search_window_vertices, 1));
        if gap_cost > max_gap_cost
%             for idx = 2:(length(shortest_path) - 1)
%                 coords = search_window_vertices(shortest_path(idx),:);
%                 rejected_connections(coords(2), coords(1)) = 2;
%             end
%             fprintf('--- Gap filling rejected with cost %f\n', gap_cost)
            continue
        end
        
        for idx = 2:(length(shortest_path) - 1)
            coords = search_window_vertices(shortest_path(idx),:);
            rebuilt_cyto_skeleton(coords(2), coords(1)) = 1;
        end
    end
end
filled_cyto_skeleton = rebuilt_cyto_skeleton;

% tmp_for_display(:, :, 1) = uint8(cyto_mip);
% tmp_for_display(:, :, 2) = 128 * uint8(rejected_connections);
% tmp_for_display(:, :, 3) = 255 * uint8(mask_cell);
% figure, imagesc(rejected_connections), title('Rejected Connections (For Debugging Purposes)')

tmp_for_display(:, :, 1) = uint8(cyto_mip);
tmp_for_display(:, :, 2) = 255 * uint8(filled_cyto_skeleton);
tmp_for_display(:, :, 3) = 255 * uint8(mask_cell);
figure, imagesc(tmp_for_display), title('Skeleton with Gapfilling on Image')

%% Next steps
toc, fprintf('Next step(s): Reconnection to Membrane\n')

%% Reconnection to the membrane
filled_cyto_skeleton(mask_cell > 0) = 0;

max_distance_to_membrane = 10;

distance_map = double(bwdist(mask_cell, 'euclidean'));
skeleton_connected_cmp = double(labelmatrix(bwconncomp(filled_cyto_skeleton)));
n_connected_cmp = max(skeleton_connected_cmp(:));

ep_on_membrane_side = double([]);
for label = 1:n_connected_cmp
    [skeleton_rows, skeleton_cols] = find(skeleton_connected_cmp == label);
    sorted_skeleton_cols = sortrows([skeleton_cols, skeleton_rows]);
    coords = double(sorted_skeleton_cols(1,:));
    if (distance_map(coords(2), coords(1)) < max_distance_to_membrane) && ...
           (mask_cyto(coords(2), coords(1)) == 1)
        ep_on_membrane_side = [ep_on_membrane_side; coords];
    end
end

weighted_map = double(cyto_mip) + 1.0;
weighted_map(mask_cell > 0) = Inf;

for ep_idx = 1:size(ep_on_membrane_side, 1)
    row = ep_on_membrane_side(ep_idx, 2);
    col = ep_on_membrane_side(ep_idx, 1);

    while true
        if (row == 1) || (row == cyto_mip_dim(1))
            break
        end        
        if isinf(weighted_map(row, col))
            break
        end
        
        neighbors_in_cyto = filled_cyto_skeleton((row-1):(row+1), col-1);
        if all(neighbors_in_cyto == 1)
            break
        end

        neighbors_weights = weighted_map((row-1):(row+1), col-1);
        if any(isinf(neighbors_weights))
            break
        end
        
        [~, idx] = max(neighbors_weights);
        idx = idx(1);
        if idx == 1
            row = row - 1;
       %elseif idx == 2 % row left unchanged
        elseif idx == 3
            row = row + 1;
        end
        col = col - 1;
        filled_cyto_skeleton(row, col) = 1;
    end
end

tmp_for_display(:, :, 1) = uint8(cyto_mip);
tmp_for_display(:, :, 2) = 255 * uint8(filled_cyto_skeleton);
tmp_for_display(:, :, 3) = 255 * uint8(mask_cell);
figure, imagesc(tmp_for_display), title('Reconnection to Membrane')

%% Removing unconnected branches
skeleton_connected_cmp = double(labelmatrix(bwconncomp(filled_cyto_skeleton)));

ep_on_membrane_side = double([]);
for label = 1:max(skeleton_connected_cmp(:))
    [skeleton_rows, skeleton_cols] = find(skeleton_connected_cmp == label);
    sorted_skeleton_cols = sortrows([skeleton_cols skeleton_rows]);
    ep_on_membrane_side = [ep_on_membrane_side; sorted_skeleton_cols(1,:), label];
end

for ep_idx = 1:size(ep_on_membrane_side, 1)
    end_point = ep_on_membrane_side(ep_idx,:);
    if dilated_mask(end_point(2), end_point(1)) == 0
        filled_cyto_skeleton(skeleton_connected_cmp == end_point(3)) = 0;
    end
end

final_skeleton_for_display(:, :, 1) = uint8(cyto_mip);
final_skeleton_for_display(:, :, 2) = 255 * uint8(filled_cyto_skeleton);
final_skeleton_for_display(:, :, 3) = 255 * uint8(mask_cell);
figure, imagesc(final_skeleton_for_display), title('Detected Cytonemes (after removal of unconnected trees)')

%% Next steps
toc, fprintf('Next step(s): Measurements and Output\n')

%% Labelling
skeleton_connected_cmp = double(labelmatrix(bwconncomp(filled_cyto_skeleton)));

%% Measures
measures = regionprops(skeleton_connected_cmp, 'Area');
tree_lengths = [measures.Area];
median_tree_length = median(tree_lengths);

measures = double([]);
for label = 1:max(skeleton_connected_cmp(:))
    measures = [measures; label, tree_lengths(label)];
end
table_of_lengths = array2table(measures, 'VariableNames', {'Label', 'Length'}); % Length of cytonemes (Pixels)

horiz_dist_to_membrane = double([]);
for label = 1:max(skeleton_connected_cmp(:))
    [skeleton_rows, skeleton_cols] = find(skeleton_connected_cmp == label);
    sorted_skeleton_cols = sortrows([skeleton_cols skeleton_rows]);
    distance = sorted_skeleton_cols(end, 1) - sorted_skeleton_cols(1, 1);% First and last points of each label
    horiz_dist_to_membrane = [horiz_dist_to_membrane; label, distance];
end
median_h_dist = median(horiz_dist_to_membrane(:, 2));
table_of_h_dist = array2table(horiz_dist_to_membrane, 'VariableNames', {'Label', 'Distance'});

n_branches_per_tree = double([]);
for label = 1:max(skeleton_connected_cmp(:))
    skeleton_tree = skeleton_connected_cmp == label;
    [~, ~, junction_coords] = anaskel(skeleton_tree);
    junction_lin_idc = sub2ind(cyto_mip_dim, junction_coords(2, :), junction_coords(1, :));
    skeleton_tree(junction_lin_idc) = 0;
    tree_connected_cmp = labelmatrix(bwconncomp(double(skeleton_tree)));
    n_branches_per_tree = [n_branches_per_tree; label, max(tree_connected_cmp(:))];
end
mean_n_branches = mean(n_branches_per_tree(:, 2));
table_of_n_branches = array2table(n_branches_per_tree, 'VariableNames', {'Label', 'Number_of_branches'});

%% Registration of figures
result_folder = ['C:\Users\eric\Desktop\Results', '-', strrep(strrep(datestr(datetime), ' ', '-'), ':', '-')];
mkdir(result_folder);
cd(result_folder);

imwrite(final_skeleton_for_display, 'cytonemes.png')
writetable(table_of_h_dist, 'distances_to_membrane.txt')
writetable(table_of_lengths, 'cytonemes_lengths.txt')
writetable(table_of_n_branches, 'cytonemes_branches.txt')

%% Display Results
disp({'---------------'
      'Cytoneme detection'
      '--------------'
     ['Total number = ',                     num2str(max(skeleton_connected_cmp(:)))];
     ['Average number of branches = ',       num2str(mean_n_branches)];
     ['Median length of cytonemes = ',       num2str(median_tree_length)];
     ['Median distance to the membrane =  ', num2str(median_h_dist)];
      '---------------'})

toc
